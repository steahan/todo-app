'use strict';

/**
 * @ngdoc function
 * @name yoTodoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yoTodoApp
 */
angular.module('yoTodoApp')
  .controller('MainCtrl', function ($scope, localStorageService) {

  	var todosInStore = localStorageService.get('todos');
  	$scope.todos = todosInStore && todosInStore.split('\n') || [];

  	// watch the list and applies changes to scope
  	$scope.$watch('todos', function () {
  		localStorageService.add('todos', $scope.todos.join('\n'));
  	}, true);

  	// needs more error checking, but will do for now
  	$scope.addTodo = function() {
  		$scope.todos.push($scope.todo); //pushes on the data bound element to the array
  		$scope.todo = ''; // and sets it to the empy string
  	};

  	$scope.removeTodo = function(index) {
  		$scope.todos.splice(index, 1);
  	};
  });
